use async_trait::async_trait;
use rocket::serde::json::serde_json;
use rocket::tokio::fs::{File, OpenOptions};
use rocket::tokio::io::{AsyncReadExt, AsyncWriteExt};
use rocket::{error, info, tokio};
use serde::{Deserialize, Serialize};
use std::time::Duration;

use crate::cache_path;

#[async_trait]
pub trait VelibFile<Data: for<'a> Deserialize<'a> + Serialize + core::fmt::Debug>:
    for<'a> Deserialize<'a> + core::fmt::Debug
{
    fn file_name() -> &'static str;
    fn api_url() -> &'static str;
    fn body_to_data(&self) -> &Vec<Data>;

    async fn file(write: bool) -> Result<File, std::io::Error> {
        OpenOptions::new()
            .read(!write)
            .write(write)
            .create(write)
            .append(false)
            .open(cache_path().join(Self::file_name()))
            .await
    }

    async fn from_file() -> Option<Vec<Data>> {
        let mut file = match Self::file(false).await {
            Ok(f) => f,
            Err(error) => {
                error!("[{}] {}", Self::file_name(), error);

                return None;
            }
        };

        let mut data = String::new();
        if let Err(error) = file.read_to_string(&mut data).await {
            error!("[{}] {}", Self::file_name(), error);

            return None;
        }

        Some(serde_json::from_str(&data).unwrap())
    }

    async fn save_data() -> Result<(), std::io::Error> {
        let body: Self = serde_json::from_str(
            &reqwest::get(Self::api_url())
                .await
                .unwrap()
                .text()
                .await
                .unwrap(),
        )
        .unwrap();

        let data = match serde_json::to_string(&body.body_to_data()) {
            Ok(string) => string.into_bytes(),
            Err(err) => {
                error!(
                    "[{}] {}, couldn't serialiaze {:#?}",
                    Self::file_name(),
                    err,
                    &body
                );
                return Ok(());
            }
        };

        Self::file(true).await?.write_all(&data).await?;
        info!("[{}] saved", Self::file_name());

        Ok(())
    }

    async fn tokio_spawn_cron(duration: Duration) {
        tokio::spawn(async move {
            let mut interval = tokio::time::interval(duration);
            loop {
                interval.tick().await;
                #[cfg(not(debug_assertions))]
                if let Err(error) = Self::save_data().await {
                    error!("[{}] {:?}", Self::file_name(), error);
                }
            }
        });
    }
}

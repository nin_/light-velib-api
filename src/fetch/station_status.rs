use super::velib_file::VelibFile;
use rocket::serde::{Deserialize, Serialize};

const FILE_NAME: &str = "station_status.json";

const URL: &str =
    "https://velib-metropole-opendata.smovengo.cloud/opendata/Velib_Metropole/station_status.json";

#[derive(Deserialize, Debug)]
pub struct StationsStatuses {
    data: Data,
}

impl VelibFile<StationStatus> for StationsStatuses {
    fn file_name() -> &'static str {
        FILE_NAME
    }

    fn api_url() -> &'static str {
        URL
    }

    fn body_to_data(&self) -> &Vec<StationStatus> {
        &self.data.stations
    }
}

#[derive(Deserialize, Debug)]
struct Data {
    stations: Vec<StationStatus>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct StationStatus {
    #[serde(rename = "station_id")]
    pub id: u64,
    pub num_docks_available: u32,
    pub last_reported: u64,
}

impl StationStatus {
    pub async fn get(id: u64) -> Option<StationStatus> {
        if let Some(stations) = StationsStatuses::from_file().await {
            for station in stations.into_iter() {
                if station.id == id {
                    return Some(station);
                }
            }
        }

        None
    }
}

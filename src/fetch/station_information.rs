use async_trait::async_trait;
use rocket::serde::{Deserialize, Serialize};

use super::velib_file::VelibFile;

const FILE_NAME: &str = "station_information.json";

const URL: &str =
    "https://velib-metropole-opendata.smovengo.cloud/opendata/Velib_Metropole/station_information.json";

#[derive(Deserialize, Debug)]
pub struct StationsInformations {
    data: Data,
}

#[async_trait]
impl VelibFile<StationInformation> for StationsInformations {
    fn file_name() -> &'static str {
        FILE_NAME
    }

    fn api_url() -> &'static str {
        URL
    }

    fn body_to_data(&self) -> &Vec<StationInformation> {
        &self.data.stations
    }
}

#[derive(Deserialize, Debug)]
struct Data {
    stations: Vec<StationInformation>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct StationInformation {
    #[serde(rename = "station_id")]
    pub id: u64,
    pub name: String,
}

use light_velib_api::fetch::station_information::StationsInformations;
use light_velib_api::fetch::station_status::StationStatus;
use light_velib_api::fetch::velib_file::VelibFile;
use light_velib_api::response::responder::FileResponder;
use light_velib_api::response::station::get::Get;

/// Get all stations ids and names
#[get("/")]
pub async fn index() -> Result<FileResponder, std::io::Error> {
    StationsInformations::file(false)
        .await
        .map(FileResponder::from)
}

/// Get a station current capacity
#[get("/<id>/capacity")]
pub async fn get_capacity(id: u64) -> Option<Get> {
    StationStatus::get(id).await.map(Get::from)
}

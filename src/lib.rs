use rocket::tokio::fs;
use std::path::PathBuf;

pub mod cors;
pub mod fetch;
pub mod response;

pub fn cache_path() -> PathBuf {
    dirs::cache_dir()
        .unwrap_or(".".into())
        .join("light-velib-api")
}

pub async fn create_cache_path() {
    let cache_path = cache_path();

    if !std::path::Path::new(&cache_path).exists() {
        fs::create_dir(cache_path).await.unwrap();
    }
}

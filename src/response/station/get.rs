use crate::fetch::station_status::StationStatus;
use crate::response::responder::get_cache_control_value;
use rocket::request::Request;
use rocket::response::{Responder, Result};
use rocket::serde::json::Json;
use rocket::serde::Serialize;
use rocket::Response;

#[derive(Serialize, Hash)]
pub struct Get {
    pub num_docks_available: u32,
    pub last_reported: u64,
}

impl<'r> Responder<'r, 'static> for Get {
    fn respond_to(self, req: &'r Request<'_>) -> Result<'static> {
        Response::build_from(Json(&self).respond_to(req)?)
            .raw_header("Cache-Control", get_cache_control_value(5 * 60))
            .ok()
    }
}

impl From<StationStatus> for Get {
    fn from(item: StationStatus) -> Self {
        Get {
            num_docks_available: item.num_docks_available,
            last_reported: item.last_reported,
        }
    }
}

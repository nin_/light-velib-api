use rocket::http::ContentType;
use rocket::request::Request;
use rocket::response::{Responder, Response, Result};
use rocket::tokio::fs::File;

pub fn get_cache_control_value(cache_time: u32) -> String {
    "public, max-age=".to_owned() + &cache_time.to_string()
}

pub struct FileResponder {
    pub file: File,
    max_age: u32,
}

impl<'r> Responder<'r, 'static> for FileResponder {
    fn respond_to(self, req: &'r Request<'_>) -> Result<'static> {
        Response::build_from(self.file.respond_to(req)?)
            .header(ContentType::JSON)
            .raw_header("Cache-Control", get_cache_control_value(self.max_age))
            .ok()
    }
}

impl From<File> for FileResponder {
    fn from(file: File) -> Self {
        FileResponder {
            file,
            max_age: 5 * 60,
        }
    }
}

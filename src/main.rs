use std::time::Duration;

use light_velib_api::cors::CORS;
use light_velib_api::create_cache_path;
use light_velib_api::fetch::station_information::StationsInformations;
use light_velib_api::fetch::station_status::StationsStatuses;
use light_velib_api::fetch::velib_file::VelibFile;
use rocket::tokio;

#[macro_use]
extern crate rocket;
extern crate dirs;

mod controllers;

#[launch]
#[tokio::main]
async fn rocket() -> _ {
    create_cache_path().await;
    StationsInformations::tokio_spawn_cron(Duration::from_secs(60 * 60)).await;
    StationsStatuses::tokio_spawn_cron(Duration::from_secs(60 * 5)).await;

    rocket::build()
        .mount(
            "/api/v0/stations",
            routes![
                controllers::station::index,
                controllers::station::get_capacity
            ],
        )
        .attach(CORS)
}

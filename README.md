# light-velib-api

Serveur HTTP qui permet de faire des requetes simples sur les données ouvertes de Velib.

## Routes

Cette API est en `v0` : des _breaking changes_ peuvent etre introduits sans changement de version.

### `/api/v0/stations/`

Retourne la liste de toutes les stations sous ce format :

```json
[
  { "station_id": 213688169, "name": "Benjamin Godard - Victor Hugo" },
  { "station_id": 653222953, "name": "Mairie de Rosny-sous-Bois" },
  { "station_id": 17278902806, "name": "Rouget de L'isle - Watteau" }
]
```

Mis à jour depuis les serveurs de _Vélib' Métropole_ une fois par heure.

### `/api/v0/stations/<id>/capacity`

Remplacez `<id>` par un `station_id`.

Retourne le nombre de place de la station dans ce format :

```json
{
  "num_docks_available": 15,
  "last_reported": 1699956033
}
```

Mis à jour depuis les serveurs de _Vélib' Métropole_ tous les 5 minutes.

`last_reported` indique le timestamp de la dernière mise-à-jour des données Velib pour cette station.

## données _Vélib' Métropole_

Les données ouvertes de Velib sont sous licence "Licence Ouverte / Open License".
Plus d'information sur [https://www.velib-metropole.fr/donnees-open-data-gbfs-du-service-velib-metropole](https://www.velib-metropole.fr/donnees-open-data-gbfs-du-service-velib-metropole)

## Contact

You can send me an email here : [lists.sr.ht/~nin0/public-inbox](https://lists.sr.ht/~nin0/public-inbox) (archives are not public).
